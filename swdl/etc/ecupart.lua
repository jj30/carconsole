--[[
  ECU Part number table. this file is managed by  Software Project Leads (project teams).
   ...
--]]

ecu_part_number = {}

-- MY14.5 UF V2
ecu_part_number["68190253"] = "68190253AJ" -- MY14.5 VP3 MEX
ecu_part_number["68190259"] = "68190259AJ" -- MY14.5 VP4 MEX
ecu_part_number["68190256"] = "68190256AF" -- MY14.5 VP3 NA
ecu_part_number["68190260"] = "68190260AJ" -- MY14.5 VP4 NA
ecu_part_number["68190264"] = "68190264AJ" -- MY14.5 VP3 CAN
ecu_part_number["68190263"] = "68190263AJ" -- MY14.5 VP4 CAN

-- MY15 LA V2
ecu_part_number["68206389"] = "68206389AJ" -- MY15 VP3 NA
ecu_part_number["68206388"] = "68206388AJ" -- MY15 VP4 NA
ecu_part_number["68206391"] = "68206391AJ" -- MY15 VP3 MEX
ecu_part_number["68206392"] = "68206392AJ" -- MY15 VP4 MEX
ecu_part_number["68206393"] = "68206393AI" -- MY15 VP3 ROW
ecu_part_number["68269795"] = "68269795AB" -- MY15 VP3 ROW w/o DAB
ecu_part_number["68206390"] = "68206390AI" -- MY15 VP4 ROW
ecu_part_number["68206395"] = "68206395AJ" -- MY15 VP3 CAN
ecu_part_number["68206394"] = "68206394AJ" -- MY15 VP4 CAN
ecu_part_number["68212744"] = "68212744AF" -- MY15 VP2H w/SDARS
ecu_part_number["68212745"] = "68212745AF" -- MY15 VP2H w/o SDARS
ecu_part_number["68212746"] = "68212746AF" -- MY15 VP2H ROW

-- MY15 DS/WK/KL V2
ecu_part_number["68238621"] = "68238621AI" -- MY15 VP3 NA
ecu_part_number["68238619"] = "68238619AI" -- MY15 VP4 NA
ecu_part_number["68238624"] = "68238624AI" -- MY15 VP3 MEX
ecu_part_number["68238625"] = "68238625AI" -- MY15 VP4 MEX
ecu_part_number["68242384"] = "68242384AH" -- MY15 VP3 BUX
ecu_part_number["68242385"] = "68242385AH" -- MY15 VP4 BUX
ecu_part_number["68242382"] = "68242382AH" -- MY15 VP3 ROW
ecu_part_number["68269796"] = "68269796AB" -- MY15 VP3 ROW w/o DAB
ecu_part_number["68242383"] = "68242383AH" -- MY15 VP4 ROW
ecu_part_number["68238627"] = "68238627AI" -- MY15 VP3 CAN
ecu_part_number["68238626"] = "68238626AI" -- MY15 VP4 CAN

-- MY15 WD V2
ecu_part_number["68239115"] = "68239115AI" -- MY15 VP3 NA
ecu_part_number["68239117"] = "68239117AI" -- MY15 VP4 NA
ecu_part_number["68239114"] = "68239114AI" -- MY15 VP3 MEX
ecu_part_number["68239116"] = "68239116AI" -- MY15 VP4 MEX
ecu_part_number["68247593"] = "68247593AH" -- MY15 VP4 ROW
ecu_part_number["68245058"] = "68245058AH" -- MY15 VP4 ROW
ecu_part_number["68239119"] = "68239119AI" -- MY15 VP3 CAN
ecu_part_number["68239118"] = "68239118AI" -- MY15 VP4 CAN

-- MY15 LX V2
ecu_part_number["68257282"] = "68257282AC" -- MY15 VP3 NA (Discontinued part)
ecu_part_number["68257281"] = "68257281AC" -- MY15 VP4 NA (Discontinued part)
ecu_part_number["68257284"] = "68257284AC" -- MY15 VP3 MEX (Discontinued part)
ecu_part_number["68257285"] = "68257285AC" -- MY15 VP4 MEX (Discontinued part)
ecu_part_number["68234426"] = "68234426AG" -- MY15 VP4 EU
ecu_part_number["68234425"] = "68234425AG" -- MY15 VP4 EU
ecu_part_number["68257286"] = "68257286AC" -- MY15 VP4 ROW (Discontinued part)
ecu_part_number["68257283"] = "68257283AC" -- MY15 VP4 ROW (Discontinued part)
ecu_part_number["68257288"] = "68257288AC" -- MY15 VP3 CAN (Discontinued part)
ecu_part_number["68257287"] = "68257287AC" -- MY15 VP4 CAN (Discontinued part)

-- MY15 Asia V2
ecu_part_number["68265331"] = "68265331AC" -- MY15 VP3 China WK KL
ecu_part_number["68224021"] = "68224021AE" -- MY15 VP4 China LX
ecu_part_number["68265335"] = "68265335AC" -- MY15 VP3 Korea WK KL
ecu_part_number["68224023"] = "68224023AE" -- MY15 VP4 Korea LX
ecu_part_number["68238656"] = "68238656AE" -- MY15 VP4 Korea UF
ecu_part_number["68265336"] = "68265336AA" -- MY15 VP4 Japan WK KL
ecu_part_number["68224022"] = "68224022AD" -- MY15 VP4 Japan LX

-- MY16 RU
ecu_part_number["68232028"] = "68232028AA" -- MY16 VP3 NA
ecu_part_number["68232026"] = "68232026AA" -- MY16 VP4 NA
ecu_part_number["68232033"] = "68232033AA" -- MY16 VP3 MEX
ecu_part_number["68232034"] = "68232034AA" -- MY16 VP4 MEX
ecu_part_number["68232027"] = "68232027AA" -- MY16 VP4 EU
ecu_part_number["68232035"] = "68232035AA" -- MY16 VP3 ROW
ecu_part_number["68232029"] = "68232029AA" -- MY16 VP4 ROW
ecu_part_number["68232037"] = "68232037AA" -- MY16 VP3 CAN
ecu_part_number["68232036"] = "68232036AA" -- MY16 VP4 CAN
ecu_part_number["68234476"] = "68234476AA" -- MY16 VP4 China
ecu_part_number["68234477"] = "68234477AA" -- MY16 VP4 Korea

-- MY16 WK/KL/DS/ZD
ecu_part_number["68258671"] = "68258671AA" -- MY16 VP3 NA
ecu_part_number["68249986"] = "68249986AA" -- MY16 VP4 NA
ecu_part_number["68249987"] = "68249987AA" -- MY16 VP3 MEX
ecu_part_number["68258673"] = "68258673AA" -- MY16 VP4 MEX
ecu_part_number["68258359"] = "68258359AA" -- MY16 VP4 EU
ecu_part_number["68258678"] = "68258678AA" -- MY16 VP4 EU
ecu_part_number["68258358"] = "68258358AA" -- MY16 VP3 ROW
ecu_part_number["68258677"] = "68258677AA" -- MY16 VP4 ROW
ecu_part_number["68258676"] = "68258676AA" -- MY16 VP3 CAN
ecu_part_number["68258675"] = "68258675AA" -- MY16 VP4 CAN
ecu_part_number["68267475"] = "68267475AA" -- MY16 VP4 China
ecu_part_number["68267477"] = "68267477AA" -- MY16 VP4 Japan
ecu_part_number["68267476"] = "68267476AA" -- MY16 VP4 Korea

-- MY16 WD
ecu_part_number["68258380"] = "68258380AA" -- MY16 VP3 NA
ecu_part_number["68258382"] = "68258382AA" -- MY16 VP4 NA
ecu_part_number["68258379"] = "68258379AA" -- MY16 VP3 MEX
ecu_part_number["68258381"] = "68258381AA" -- MY16 VP4 MEX
ecu_part_number["68258386"] = "68258386AA" -- MY16 VP3 ROW
ecu_part_number["68258385"] = "68258385AA" -- MY16 VP4 ROW
ecu_part_number["68258384"] = "68258384AA" -- MY16 VP3 CAN
ecu_part_number["68258383"] = "68258383AA" -- MY16 VP4 CAN

-- MY16 LX
ecu_part_number["68267509"] = "68267509AA" -- MY16 VP4 China
ecu_part_number["68267508"] = "68267508AA" -- MY16 VP4 Japan
ecu_part_number["68267506"] = "68267506AA" -- MY16 VP4 Korea

-- MY16 UF
ecu_part_number["68267507"] = "68267507AA" -- MY16 VP4 Korea

