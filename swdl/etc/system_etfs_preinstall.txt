###############################################################################
# This is on for cleaning up APPS file. 
# This file contains the list of files or directories which needs to be removed, 
# created or establish a 
# link during software update.
# example:-
# [remove] 
# /fs/etfs/db*
#
# [create] 
# /fs/etfs/sdars/
# /fs/etfs/VERBOSE_STARTUP
# 
# [link]
# /fs/etfs/sdars/, /fs/etfs/sdars_tmp
###############################################################################

# Remove files we plan on removing before backing up anything
# This section should be identical to system_etfs_postinstall.txt
[remove]
/fs/etfs/bin
/fs/etfs/lib
/fs/etfs/root
/fs/etfs/etc
/fs/etfs/hmi
/fs/etfs/var
/fs/etfs/wicome
/fs/etfs/usr/var/qdb/mme*
/fs/etfs/usr/NNG_save/save
/fs/etfs/NDRToJSON.bin
/fs/etfs/usr/var/pps/
# SDARS: Delete the data service databases that
# are contained within their respective directories.
/fs/etfs/usr/var/sdars/channelart/*
/fs/etfs/usr/var/sdars/fuel/*
/fs/etfs/usr/var/sdars/movies/*
/fs/etfs/usr/var/sdars/phonetics/*
/fs/etfs/usr/var/sdars/phonetics/rec/*
/fs/etfs/usr/var/sdars/phonetics/tts/*
/fs/etfs/usr/var/sdars/rfd/*
/fs/etfs/usr/var/sdars/sports/*
/fs/etfs/usr/var/sdars/sportsservice/*
/fs/etfs/usr/var/sdars/traffic/*
/fs/etfs/usr/var/sdars/weather/*
# SDARS: Delete every sub-directory under DriverA/B.
# Do not delete the file 'sms.cfg' in DriverA/B
/fs/etfs/usr/var/sdars/DriverA/channelart
/fs/etfs/usr/var/sdars/DriverA/fuel
/fs/etfs/usr/var/sdars/DriverA/movies
/fs/etfs/usr/var/sdars/DriverA/phonetics/rec
/fs/etfs/usr/var/sdars/DriverA/phonetics/tts
/fs/etfs/usr/var/sdars/DriverA/phonetics
/fs/etfs/usr/var/sdars/DriverA/rfd
/fs/etfs/usr/var/sdars/DriverA/sports
/fs/etfs/usr/var/sdars/DriverA/sportsservice
/fs/etfs/usr/var/sdars/DriverA/traffic
/fs/etfs/usr/var/sdars/DriverA/weather  
/fs/etfs/usr/var/sdars/DriverB/channelart
/fs/etfs/usr/var/sdars/DriverB/fuel
/fs/etfs/usr/var/sdars/DriverB/movies
/fs/etfs/usr/var/sdars/DriverB/phonetics/rec
/fs/etfs/usr/var/sdars/DriverB/phonetics/tts
/fs/etfs/usr/var/sdars/DriverB/phonetics
/fs/etfs/usr/var/sdars/DriverB/rfd
/fs/etfs/usr/var/sdars/DriverB/sports
/fs/etfs/usr/var/sdars/DriverB/sportsservice
/fs/etfs/usr/var/sdars/DriverB/traffic
/fs/etfs/usr/var/sdars/DriverB/weather  
# rec and tts are directories that should have been placed under phonetics. Delete these old versions.
/fs/etfs/usr/var/sdars/DriverA/rec  
/fs/etfs/usr/var/sdars/DriverA/tts
/fs/etfs/usr/var/sdars/DriverB/rec  
/fs/etfs/usr/var/sdars/DriverB/tts  
/fs/etfs/usr/var/sdars/rec
/fs/etfs/usr/var/sdars/tts
/fs/etfs/usr/var/speech_service/dialog/grammar/*
/fs/etfs/usr/var/speechTEFiles/apps/*
/fs/etfs/usr/var/speechTEFiles/extPhones/*
/fs/etfs/usr/var/speechTEFiles/mediaOutOfSync/*
/fs/etfs/usr/var/speechTEFiles/sat/*
/fs/etfs/usr/var/audioMgt/*
/fs/etfs/usr/var/iPodTags

[backup]
/fs/etfs/usr


