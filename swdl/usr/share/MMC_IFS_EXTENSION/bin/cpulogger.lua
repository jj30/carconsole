--following utilities will be started on SD or USB pluggin
--1. MonitorProcesses
--2. HCP client
--following utilities will be stoped on ejection
--1. MonitorProcesses


local mcd = require "mcd"
local mcdInsertRule_1 = "CPULOGGER"
local mcdInsertRule_2 = "HCPClient_ON"
local mcdEjectRule = "EJECTED"
local setMonProcDevicePath
local alreadyLogging = 0

local function beginLogging(path)
   if alreadyLogging == 0 then
      alreadyLogging = 1
	  
      setMonProcDevicePath = path
      -----local cpulogger
      print("Begin cpulogger, logging to: ", path)
      --print "Entered Begin logging from Lua script\n"

      --------- Executing the logging script-----

      os.execute("sh /fs/mmc0/app/bin/cpulogger.sh")  
   end
end

--function for executing startHCP client
local function startHCPClient()
    print "cpulogger:Entered start HCPClient logging from Lua script\n"
    os.execute("sh /fs/mmc0/app/bin/runhcpclient.sh")
end

local function stopLogging(path)
   if path == setMonProcDevicePath then
      print ("Stopped logging to path:", path)
      os.execute("slay MonitorProcesses")
      os.execute("slay dumper")
	  -- Reset logging flag
	  alreadyLogging = 0
   end
end   
 
--Set Notfications for SD INSERTION
--print "calling mcd.notify\n"
mcd.notify(mcdInsertRule_1,beginLogging)
mcd.notify(mcdInsertRule_2,startHCPClient)
mcd.notify(mcdEjectRule,stopLogging)

--print "called mcd.notify\n"
