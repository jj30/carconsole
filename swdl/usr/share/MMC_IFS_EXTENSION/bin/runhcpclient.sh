#!/bin/sh
echo "\n#### runhcpclient.sh: called ####\n"  > $CONSOLE_DEVICE

if [ -e /tmp/HCPrunning ]; then
   echo "\n#### runhcpclient.sh: already running ####\n"  > $CONSOLE_DEVICE
   return 0
else
    touch /tmp/HCPrunning
	echo "starting ring buffer" > $CONSOLE_DEVICE
	/fs/mmc0/app/bin/ring_buffer -R /dev/dbus_buffer -B 102400 &
	
	# WAIT default 5 seconds for the path to appear
	qwaitfor /dev/dbus_buffer 
	
	echo "starting dbus-monitor" > $CONSOLE_DEVICE
	/fs/mmc0/app/bin/dbus-monitor > /dev/dbus_buffer &

/fs/mmc0/app/bin/HCPMonitor -c /fs/etfs/usr/var/hcp/HCPConfig.conf &
fi
