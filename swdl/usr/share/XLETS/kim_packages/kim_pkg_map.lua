--[[
   KIM0 => no KIM package
   KIM1 => KIM package 1
   KIM2 => KIM package 2
   ...
--]]

kim_pkg_map = {}

-- MY 13
kim_pkg_map["05091051"] = "KIM1" -- MY13 VP4 NA
kim_pkg_map["05091053"] = "KIM2" -- MY13 VP4 EU
kim_pkg_map["05091054"] = "KIM1" -- MY13 VP3 NA
kim_pkg_map["05091055"] = "KIM0" -- MY13 VP4 ROW
kim_pkg_map["05091056"] = "KIM2" -- MY13 VP3 EU
kim_pkg_map["05091058"] = "KIM0" -- MY13 VP3 MEX
kim_pkg_map["05091060"] = "KIM0" -- MY13 VP4 MEX
kim_pkg_map["68088691"] = "KIM0" -- ???
kim_pkg_map["68088692"] = "KIM0" -- MY13 VP4 CH
kim_pkg_map["68088693"] = "KIM0" -- MY13 VP3 CH
kim_pkg_map["68088696"] = "KIM2" -- MY13 VP4 KR
kim_pkg_map["68195576"] = "KIM5" -- MY13 VP4 CH
kim_pkg_map["68088697"] = "KIM2" -- MY13 VP4 JP
kim_pkg_map["68140964"] = "KIM0" -- MY13 VP3 ROW
kim_pkg_map["68150036"] = "KIM0" -- MY13 VP2 NA w/ SDARS
kim_pkg_map["68150037"] = "KIM0" -- MY13 VP2 NA w/o SDARS
kim_pkg_map["68150039"] = "KIM0" -- MY13 VP2 ECE
kim_pkg_map["68150060"] = "KIM0" -- MY13 VP2 ROW
kim_pkg_map["68184965"] = "KIM0" -- MY13 VP4 CAN
kim_pkg_map["68184966"] = "KIM0" -- MY13 VP3 CAN

-- MY 13.5
kim_pkg_map["68190240"] = "KIM1" -- MY13.5 VP4 NA
kim_pkg_map["68190241"] = "KIM2" -- MY13.5 VP4 EU
kim_pkg_map["68190242"] = "KIM1" -- MY13.5 VP3 NA
kim_pkg_map["68190243"] = "KIM2" -- MY13.5 VP4 ROW
kim_pkg_map["68190244"] = "KIM2" -- MY13.5 VP3 EU
kim_pkg_map["68190245"] = "KIM0" -- MY13.5 VP3 MEX
kim_pkg_map["68190246"] = "KIM0" -- MY13.5 VP4 MEX
kim_pkg_map["68190247"] = "KIM2" -- MY13.5 VP3 ROW
kim_pkg_map["68190248"] = "KIM0" -- MY13.5 VP3 CAN
kim_pkg_map["68190249"] = "KIM0" -- MY13.5 VP4 CAN

-- MY 13.75 KL
kim_pkg_map["68211955"] = "KIM1" -- MY13.75 VP4 NA
kim_pkg_map["68211958"] = "KIM0" -- MY13.75 VP4 EU
kim_pkg_map["68211959"] = "KIM1" -- MY13.75 VP3 NA
kim_pkg_map["68211960"] = "KIM0" -- MY13.75 VP4 ROW
kim_pkg_map["68211961"] = "KIM0" -- MY13.75 VP3 EU
kim_pkg_map["68211962"] = "KIM0" -- MY13.75 VP3 MEX
kim_pkg_map["68211963"] = "KIM0" -- MY13.75 VP4 MEX
kim_pkg_map["68211964"] = "KIM0" -- MY13.75 VP3 ROW
kim_pkg_map["68211965"] = "KIM0" -- MY13.75 VP4 CAN
kim_pkg_map["68211966"] = "KIM0" -- MY13.75 VP3 CAN

-- MY 14 WD
kim_pkg_map["68164636"] = "KIM0" -- MY14 VP3 MEX
kim_pkg_map["68164637"] = "KIM1" -- MY14 VP3 NA
kim_pkg_map["68164639"] = "KIM0" -- MY14 VP3 ROW
kim_pkg_map["68164640"] = "KIM0" -- MY14 VP4 MEX
kim_pkg_map["68164641"] = "KIM1" -- MY14 VP4 NA
kim_pkg_map["68164643"] = "KIM0" -- MY14 VP4 ROW
kim_pkg_map["68164647"] = "KIM0" -- MY14 VP4 CAN
kim_pkg_map["68193004"] = "KIM2" -- MY14 VP3 CAN
kim_pkg_map["68193005"] = "KIM2" -- MY14 VP4 CAN
kim_pkg_map["68185347"] = "KIM0" -- MY14 VP3 CAN
kim_pkg_map["68164640"] = "KIM0" -- MY14 VP3 CAN
kim_pkg_map["68185346"] = "KIM0" -- MY14 VP3 CAN

-- MY 14 DS/WK/ZD
kim_pkg_map["68224534"] = "KIM2" -- MY14 VP3 MEX
kim_pkg_map["68224535"] = "KIM2" -- MY14 VP4 MEX
kim_pkg_map["68224531"] = "KIM1" -- MY14 VP3 NA
kim_pkg_map["68224525"] = "KIM1" -- MY14 VP4 NA
kim_pkg_map["68224536"] = "KIM2" -- MY14 VP3 ROW 
kim_pkg_map["68224532"] = "KIM2" -- MY14 VP4 ROW
kim_pkg_map["68224538"] = "KIM2" -- MY14 VP3 CAN
kim_pkg_map["68224537"] = "KIM2" -- MY14 VP4 CAN
kim_pkg_map["68224533"] = "KIM2" -- MY14 VP3 EU 
kim_pkg_map["68224530"] = "KIM2" -- MY14 VP4 EU

-- MY 14.5 UF
kim_pkg_map["68190253"] = "KIM0" -- MY14.5 VP3 MEX
kim_pkg_map["68190259"] = "KIM0" -- MY14.5 VP4 MEX
kim_pkg_map["68190256"] = "KIM1" -- MY14.5 VP3 NA
kim_pkg_map["68190260"] = "KIM1" -- MY14.5 VP4 NA
kim_pkg_map["68190258"] = "KIM2" -- MY14.5 VP3 ROW 
kim_pkg_map["68190262"] = "KIM2" -- MY14.5 VP4 ROW
kim_pkg_map["68190264"] = "KIM0" -- MY14.5 VP3 CAN
kim_pkg_map["68190263"] = "KIM0" -- MY14.5 VP4 CAN
kim_pkg_map["68190257"] = "KIM2" -- MY14.5 VP3 EU
kim_pkg_map["68190261"] = "KIM2" -- MY14.5 VP4 EU

-- MY 15 LA
kim_pkg_map["68206391"] = "KIM2" -- MY15 VP3 MEX
kim_pkg_map["68206392"] = "KIM2" -- MY15 VP4 MEX
kim_pkg_map["68206389"] = "KIM1" -- MY15 VP3 NA
kim_pkg_map["68206388"] = "KIM1" -- MY15 VP4 NA
kim_pkg_map["68206393"] = "KIM2" -- MY15 VP3 ROW 
kim_pkg_map["68269795"] = "KIM2" -- MY15 VP3 ROW w/o DAB 
kim_pkg_map["68206390"] = "KIM2" -- MY15 VP4 ROW
kim_pkg_map["68206395"] = "KIM2" -- MY15 VP3 CAN
kim_pkg_map["68206394"] = "KIM2" -- MY15 VP4 CAN
kim_pkg_map["68234426"] = "KIM2" -- MY15 VP3 ECE
kim_pkg_map["68234425"] = "KIM2" -- MY15 VP4 ECE

-- MY 15 DS/WK/KL
kim_pkg_map["68238624"] = "KIM2" -- MY15 VP3 MEX
kim_pkg_map["68238625"] = "KIM2" -- MY15 VP4 MEX
kim_pkg_map["68238621"] = "KIM1" -- MY15 VP3 NA
kim_pkg_map["68238619"] = "KIM1" -- MY15 VP4 NA
kim_pkg_map["68242384"] = "KIM2" -- MY15 VP3 BUX 
kim_pkg_map["68242385"] = "KIM2" -- MY15 VP4 BUX
kim_pkg_map["68238627"] = "KIM2" -- MY15 VP3 CAN
kim_pkg_map["68238626"] = "KIM2" -- MY15 VP4 CAN
kim_pkg_map["68242382"] = "KIM2" -- MY15 VP3 ROW
kim_pkg_map["68269796"] = "KIM2" -- MY15 VP3 ROW w/o DAB
kim_pkg_map["68242383"] = "KIM2" -- MY15 VP4 ROW

-- MY 15 WD
kim_pkg_map["68239114"] = "KIM0" -- MY15 VP3 MEX
kim_pkg_map["68239116"] = "KIM0" -- MY15 VP4 MEX
kim_pkg_map["68239115"] = "KIM1" -- MY15 VP3 NA
kim_pkg_map["68239117"] = "KIM1" -- MY15 VP4 NA
kim_pkg_map["68239119"] = "KIM0" -- MY15 VP3 CAN
kim_pkg_map["68239118"] = "KIM0" -- MY15 VP4 CAN
kim_pkg_map["68247593"] = "KIM0" -- MY15 VP4 ROW
kim_pkg_map["68245058"] = "KIM0" -- MY15 VP4 ROW

--Fiat 520
kim_pkg_map["735579630"] = "KIM0" -- HU 520 EU W/O DAB 2860 FIAT C021
kim_pkg_map["735578497"] = "KIM0" -- HU 520 EU W/DAB 2860 FIAT C022
kim_pkg_map["735579631"] = "KIM0" -- HU 520 NAFTA W/O SDARS 2860 FIAT C023
kim_pkg_map["735579632"] = "KIM0" -- HU 520 NAFTA W/SDARS 2860 FIAT C024
kim_pkg_map["735601023"] = "KIM4" -- HU 520 NAFTA UC DIR 2860 FIAT E034
kim_pkg_map["735599731"] = "KIM0" -- HU 520 ROW 2860 FIAT E037
kim_pkg_map["735598175"] = "KIM0" -- HU 520 EU W/O DAB 2860 FIAT C021
kim_pkg_map["735598178"] = "KIM0" -- HU 520 EU W/DAB 2860 FIAT C022
kim_pkg_map["735598176"] = "KIM0" -- HU 520 NAFTA W/O SDARS 2860 FIAT C023
kim_pkg_map["735598177"] = "KIM0" -- HU 520 NAFTA W/SDARS 2860 FIAT C024               
kim_pkg_map["735604050"] = "KIM0" -- HU 520 EU W/O DAB 2860 FIAT C021
kim_pkg_map["735604053"] = "KIM0" -- HU 520 EU W/DAB 2860 FIAT C022
kim_pkg_map["735604051"] = "KIM0" -- HU 520 NAFTA W/O SDARS 2860 FIAT C023
kim_pkg_map["735604052"] = "KIM0" -- HU 520 NAFTA W/SDARS 2860 FIAT C024
kim_pkg_map["735611054"] = "KIM4" -- HU 520 NAFTA UC DIR 2860 FIAT E034
kim_pkg_map["735604995"] = "KIM0" -- HU 520 ROW 2860 FIAT E037
                
--Fiat 334
kim_pkg_map["735625963"] = "KIM0" -- New Add for Ryan, Ask him what it is.             
kim_pkg_map["735576588"] = "KIM0" -- HU VP4 334 NA FULL 2885 FIAT E024
kim_pkg_map["735576587"] = "KIM0" -- HU VP4 334 NA W/O SDARS 2885 FIAT E025
kim_pkg_map["735586001"] = "KIM0" -- HU VP4 334 EU W/ DAB 2885 FIAT E026
kim_pkg_map["735591816"] = "KIM0" -- HU VP4 334 ROW 2885 FIAT E027               
kim_pkg_map["735605148"] = "KIM0" -- HU VP4 334 NA w/ SDARS 2885 FIAT E046
kim_pkg_map["735605125"] = "KIM0" -- HU VP4 334 NA FULL 2885 FIAT E024 C
kim_pkg_map["735605126"] = "KIM0" -- HU VP4 334 NA W/O SDARS 2885 FIAT E025 C
kim_pkg_map["735605120"] = "KIM0" -- HU VP4 334 EU W/ DAB 2885 FIAT E026 C
kim_pkg_map["735605124"] = "KIM0" -- HU VP4 334 ROW 2885 FIAT E027 C           
kim_pkg_map["735612325"] = "KIM0" -- HU VP4 334 NA w/ SDARS 2885 FIAT E046 C
kim_pkg_map["735626526"] = "KIM0" -- HU VP4 334 NA UC MOB 2885 FIAT E024  C
kim_pkg_map["735626525"] = "KIM0" -- HU VP4 334 NA W/O SDARS 2885 FIAT E025 C
kim_pkg_map["735626527"] = "KIM0" -- HU VP4 334 EU W/ DAB 2885 FIAT E026 C
kim_pkg_map["735626531"] = "KIM0" -- HU VP4 334 ROW 2885 FIAT E027 C
kim_pkg_map["735626532"] = "KIM0" -- HU VP4 334 NA w/ SDARS 2885 FIAT E046 C

--Asia MY15
kim_pkg_map["68252212"] = "KIM5" -- MY15 VP4 CH
kim_pkg_map["68243298"] = "KIM0" -- MY15 VP4 KR
kim_pkg_map["68243299"] = "KIM0" -- MY15 VP4 JP
